---

- name: Get CNG image version
  check_mode: false
  ansible.builtin.command:
    cmd: bin/cng-versions-from-deploy-version -i {{ deploy_version }}
  register: cmd_image_version
  changed_when: false

- name: Get CNG repository tag
  check_mode: false
  ansible.builtin.command:
    cmd: bin/cng-versions-from-deploy-version -r {{ deploy_version }}
  register: cmd_repository_tag
  changed_when: false

- name: Set image version and repository tag
  ansible.builtin.set_fact:
    image_version: "{{ cmd_image_version.stdout }}"
    repository_tag: "{{ cmd_repository_tag.stdout }}"

- name: Image version and repository tag
  ansible.builtin.debug:
    msg: |
      Image version: {{ image_version }}
      Repository tag: {{ repository_tag }}

- name: Set fact for pipeline URL
  ansible.builtin.set_fact:
    pipeline_url: "{{ cng_project_api }}/pipelines?&ref={{ repository_tag | urlencode() }}"

- name: Pipeline URL
  ansible.builtin.debug:
    msg: "Checking {{ pipeline_url }} for a completed status"

- name: Get CNG pipeline
  check_mode: false
  ansible.builtin.uri:
    url: "{{ pipeline_url }}"
    method: GET
    headers:
      PRIVATE-TOKEN: "{{ dev_api_token }}"
  until:
    - cng_present_result.status == 200
  retries: 3
  delay: 5
  register: cng_present_result

- name: Set fact for whether there is a CNG tagged pipeline
  ansible.builtin.set_fact:
    cng_pipeline_present: '{{ cng_present_result.json | length > 0 }}'

- name: Check to see if CNG pipeline is complete
  check_mode: false
  ansible.builtin.uri:
    url: "{{ pipeline_url }}"
    method: GET
    headers:
      PRIVATE-TOKEN: "{{ dev_api_token }}"
  until:
    - "'json' in cng_build_result and cng_build_result.json[0].status in ['manual', 'success', 'failed', 'canceled', 'skipped']"
  # Wait 15 minutes for CNG pipeline to complete
  retries: 30
  delay: 30
  register: cng_build_result
  when:
    - cng_pipeline_present

- name: Set pipeline facts
  ansible.builtin.set_fact:
    status: "{{ cng_build_result.json[0].status }}"
    web_url: "{{ cng_build_result.json[0].web_url }}"
  when:
    - cng_pipeline_present

- name: Pipeline URL
  check_mode: false
  ansible.builtin.debug:
    msg: "Got pipeline URL {{ web_url }} with status '{{ status }}'"
  when:
    - cng_pipeline_present

- name: Check to see if CNG pipeline is successful, only manual or success is allowed
  check_mode: false
  ansible.builtin.fail:
    msg: "Ref {{ image_version }} in pipeline {{ web_url }} is in status {{ status }}."
  when:
    - cng_pipeline_present
    - status not in ["manual", "success"]
