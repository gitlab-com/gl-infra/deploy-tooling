---

- name: "Clean files in the deb cache directory"
  ansible.builtin.command: "find {{ deb_cache_dir }} -type f -mindepth 1 -delete"
  changed_when: true

- name: "Download deb from the package release bucket"
  ansible.builtin.debug:
    msg: |
      Running `gcloud storage cp gs://{{ deb_bucket }}/{{ deb_gcs_path }} {{ deb_local_path }}`
        vault_addr: {{ vault_addr }}
        vault_gcp_role_base: {{ vault_gcp_role_base }}
        vault_gcp_impersonated_account_pkg: {{ vault_gcp_impersonated_account_pkg }}
- name: "Fetching token used for impersonation and download deb"
  ignore_errors: true
  check_mode: false
  no_log: true  # Prevent logging to avoid leaking tokens
  block:
    - name: "Get Google Compute JWT"
      ansible.builtin.uri:
        url: "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?format=full&audience=http://vault/{{ vault_gcp_role_base }}"
        headers:
          Metadata-Flavor: Google
        return_content: true
      register: gcp_jwt

    - name: "Get Vault token"
      ansible.builtin.uri:
        url: "{{ vault_addr }}/v1/auth/gcp/login"
        method: 'POST'
        body_format: json
        body:
          jwt: "{{ gcp_jwt.content }}"
          role: "{{ vault_gcp_role_base }}"
        return_content: true
      register: vault_token

    - name: "Get GCP impersonated service account access token"
      ansible.builtin.uri:
        url: "{{ vault_addr }}/v1/gcp/impersonated-account/{{ vault_gcp_impersonated_account_pkg }}/token"
        headers:
          X-Vault-Token: "{{ vault_token.json.auth.client_token }}"
        return_content: true
      register: package_fetcher_access_token

- name: "Download deb with gcloud"
  ansible.builtin.shell:
    cmd: |
      export CLOUDSDK_AUTH_ACCESS_TOKEN={{ package_fetcher_access_token.json.data.token }}
      gcloud storage cp gs://{{ deb_bucket }}/{{ deb_gcs_path }} {{ deb_local_path }}
  ignore_errors: true
  no_log: true  # Prevent logging to avoid leaking tokens
  register: deb_download
  changed_when: true

- name: "Update the apt cache and download Omnibus from packagecloud since we could not fetch the deb from the pkgs bucket"
  when: not deb_install_enable or deb_download.failed
  block:
    - name: "Apt cache update if the gitlab-ee version is not present in the local cache"
      ansible.builtin.shell: |
        set -euf -o pipefail
        if ! apt-cache madison gitlab-ee | grep -q " {{ deploy_version }} "; then
          apt-get -q update
        fi
      args:
        executable: "/bin/bash"
      environment:
        PATH: "{% if lookup('env', 'APT_WRAPPER_ENABLED') == 'true' %}/usr/local/apt-wrapper:{% endif %}{{ lookup('env', 'PATH') }}"
      retries: 10
      check_mode: false
      until: not apt_cache_update.failed
      register: apt_cache_update
      changed_when: apt_cache_update.stdout != ""
      delay: 3

    - name: "Cache update results"
      ansible.builtin.debug:
        msg: |
          {{ apt_cache_update.cmd }}

          stdout: {{ apt_cache_update.stdout }}
          stderr: {{ apt_cache_update.stderr }}
      when:
        - apt_cache_update.stdout != ""

    - name: "Remove all but the latest gitlab-ee package from cache to free up space"
      ansible.builtin.shell: |
        set -euf -o pipefail
        find /var/cache/apt/archives/ -name gitlab-ee_* | sort | head -n -1 | xargs -r rm -f
      args:
        executable: "/bin/bash"
      check_mode: false
      register: cleanup_package_archive
      changed_when: cleanup_package_archive.rc == 0

    - name: "Warmup the omnibus package"  # noqa: command-instead-of-module
      ansible.builtin.command: "apt-get install -d -y -q --allow-downgrades --allow-unauthenticated gitlab-ee={{ deploy_version }}"
      check_mode: false
      retries: 24
      until: not warmup_omnibus.failed
      register: warmup_omnibus
      # If there are 0 bytes fetched, this is not a change
      changed_when: "'Need to get 0 B' not in warmup_omnibus.stdout"
      delay: 5

    - name: "Warmup results"
      ansible.builtin.debug:
        msg: |
          {{ warmup_omnibus.cmd }}

          stdout: {{ warmup_omnibus.stdout }}
          stderr: {{ warmup_omnibus.stderr }}
      when:
        - "'Need to get 0 B' not in warmup_omnibus.stdout"
