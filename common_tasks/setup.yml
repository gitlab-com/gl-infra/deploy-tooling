---

- name: Set rollback facts from env
  ansible.builtin.set_fact:
    rollback: "{{ lookup('env', 'DEPLOY_ROLLBACK') | default('false', true) | bool }}"

- name: "Create custom fact directory"
  check_mode: false
  ansible.builtin.file:
    path: "/etc/ansible/facts.d"
    state: "directory"

- name: "Insert custom fact file"
  check_mode: false
  ansible.builtin.copy:
    src: files/gitlab.fact
    dest: /etc/ansible/facts.d/gitlab.fact
    mode: 0755

- name: "Ensure python-apt is installed"
  check_mode: false
  ansible.builtin.apt:
    name: python-apt
  environment:
    PATH: "{% if lookup('env', 'APT_WRAPPER_ENABLED') == 'true' %}/usr/local/apt-wrapper:{% endif %}{{ lookup('env', 'PATH') }}"

- name: "Re-run setup to use custom facts"
  check_mode: false
  become: true
  ansible.builtin.setup:
    filter: ansible_local

- name: Failed to gather custom facts
  ansible.builtin.fail:
    msg: "Custom fact validation failed! Got: {{ ansible_local }}"
  when:
    - "'gitlab' not in ansible_local or 'error loading fact' in ansible_local['gitlab'] or ansible_local['gitlab'].keys()|length < 1"

- name: Set info fact
  ansible.builtin.set_fact:
    info: "{{ '*DRYRUN* ' if ansible_check_mode else '' }}*{{ lookup('env', 'CURRENT_DEPLOY_ENVIRONMENT') }} {{ lookup('env', 'DEPLOY_VERSION') }}* "
  check_mode: false

- name: Set role based facts
  ansible.builtin.set_fact:
    is_gitaly_role: "{{ (group_names | select('search', 'base_stor_gitaly') | list | count > 0) or (group_names | select('search', 'type_gitaly') | list | count > 0) }}"
    is_praefect_role: "{{ (group_names | select('search', 'base_stor_praefect') | list | count > 0) or (group_names | select('search', 'type_praefect') | list | count > 0) }}"
    is_deploy_role: "{{ (group_names | select('search', 'base_deploy_node') | list | count > 0) or (group_names | select('search', 'type_deploy') | list | count > 0) }}"

- name: Set deploy version fact
  ansible.builtin.set_fact:
    deploy_version: "{{ lookup('env', 'DEPLOY_VERSION') }}"

- name: Set deb facts
  ansible.builtin.set_fact:
    deb_cache_dir: "/var/cache/deploy-tooling/deb"
    deb_fname: "gitlab-ee_{{ deploy_version }}_amd64.deb"

- name: Get codename
  ansible.builtin.command: "lsb_release -sc"
  changed_when: false
  check_mode: false
  register: ubuntu_codename

- name: Set deb facts for GCS
  ansible.builtin.set_fact:
    deb_project: "gitlab-com-pkgs"
    deb_gcs_path: "ubuntu-{{ ubuntu_codename.stdout | trim }}/gitlab-ee_{{ deploy_version }}_amd64.deb"
    deb_local_path: "{{ deb_cache_dir }}/{{ deb_fname }}"
    deb_bucket: "gitlab-com-pkgs-release"
    deb_install_enable: "{{ lookup('env', 'DEB_INSTALL_ENABLE') | bool }}"
    vault_addr: "{{ lookup('env', 'VAULT_SERVER_URL', default='https://vault.ops.gke.gitlab.net') }}"
    vault_gcp_role_base: "chef_{{ lookup('env', 'CURRENT_DEPLOY_ENVIRONMENT') }}_base"
    vault_gcp_impersonated_account_pkg: "{{ lookup('env', 'HOST_PROJECT', default='unknown-project') }}--package-fetcher"

- name: "Create deb cache directory"
  ansible.builtin.file:
    path: "{{ deb_cache_dir }}"
    state: "directory"
  check_mode: false
