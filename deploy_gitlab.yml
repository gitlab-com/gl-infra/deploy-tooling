---

########################################################
# deploy_gitlab.yml
#
# This play is the main play that includes multiple plays
# for deploying GitLab.
#
# The following must be set in the environment:
#
#   CURRENT_DEPLOY_ENVIRONMENT : This is the environment, example: gstg
#   GITLAB_ROLES : The Ansible inventory roles (comma separated)
#                  to deploy to which is the part after the env, for example: gstg_base_fe_git
#   DEPLOY_VERSION: The omnibus version of GitLab to install, example: 11.6.0-rc2.ee.0
#

- name: Prechecks deploy
  import_playbook: prechecks_deploy.yml
- name: Setup
  import_playbook: setup.yml
- name: "Deployer for {{ lookup('env','GITLAB_ROLES') }}"
  gather_facts: false
  hosts: "{{ lookup('env','GITLAB_ROLES') | default('localhost', true) }}"
  module_defaults:
    ansible.builtin.setup:
      gather_timeout: 60
    ansible.builtin.gather_facts:
      gather_timeout: 60
  serial: 100
  max_fail_percentage: 0
  pre_tasks:
    - name: Warmup
      ansible.builtin.import_tasks: "common_tasks/warmup.yml"
      become: true
      when:
        - lookup('env', 'CURRENT_DEPLOY_ENVIRONMENT') == 'release'

    - name: "Service status for {{ inventory_hostname_short }}"
      ansible.builtin.debug:
        var: ansible_local['gitlab']

  tasks:
    - name: "{{ info }} Install GitLab"
      ansible.builtin.include_tasks: "common_tasks/install_gitlab_ee.yml"

    # Always run migrations to ensure they have been run,
    # even if the package has not been upgraded
    - name: "{{ info }} Migrate the Rails Database"
      ansible.builtin.include_tasks: "common_tasks/migrate_gitlab_ee.yml"
      when:
        - lookup('env', 'RUN_MIGRATIONS')

    - name: Export pending migrations metric
      ansible.builtin.import_tasks: "common_tasks/export_pending_migrations_metric.yml"
      when:
        - lookup('env', 'RUN_MIGRATIONS')

    - name: "{{ info }} Migrate the ClickHouse Databases"
      ansible.builtin.include_tasks: "common_tasks/clickhouse_migrate_gitlab_ee.yml"
      when:
        - lookup('env', 'RUN_MIGRATIONS')

    - name: "{{ info }} Version Check"
      ansible.builtin.include_tasks: "common_tasks/version_check.yml"
      when:
        - is_gitaly_role or is_praefect_role
        - not lookup('env', 'SKIP_WAIT_FOR_VERSION_CHANGE') == 'true'

  post_tasks:
    - name: Update completed hosts
      ansible.builtin.import_tasks: "common_tasks/update_completed_hosts.yml"
