## Summary

This repository has deploy-tooling for deploying to GitLab.com
with Ansible.
This is a replacement for the existing patching utilities for GitLab rails.

It replaces the following:

* gitlab-patcher <https://gitlab.com/gitlab-com/gl-infra/gitlab-patcher>
* post-deployment-patches <https://dev.gitlab.org/gitlab/post-deployment-patches>
* takeoff <https://gitlab.com/gitlab-org/takeoff>

### Creating or rolling back a patch for a release

Please refer to the
[release documentation for deployer](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/gitlab-com-deployer.md) and
the
[post-deployment patch documentation](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)
for more information about how this repository is used for GitLab.com
deployments.

### Development (local)

**Note**: Testing Ansible plays locally will work with proper ssh forwarding for IP
addresses for the network you are testing, however, it is recommended to use the console
server for testing as it allows you to access the appropriate network and also
will allow you to access HAProxy directly.

```bash
rtx install # or `asdf install`
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Ensure that you can generate an inventory. One MUST have a working `gcloud` to run `ansible-inventory --graph`

## The following will run a deployment in check
## mode locally on the staging deploy node
export DEPLOY_VERSION=11.7.3-ee.0
export CURRENT_DEPLOY_ENVIRONMENT=gstg
export GITLAB_ROLES=type_deploy_node:&env_gstg:&stage_main
ansible-galaxy collection install -r ansible-galaxy-requirements.yaml
ansible-inventory --graph
ansible-playbook --check deploy_gitlab.yml
```

### Local Development using the Console server

```bash
# export PATH="$HOME/.local/bin:$PATH"  # Add to .bash_profile

# Clone deploy-tooling
workspace_dir="$HOME/workspace"
mkdir $workspace_dir
cd $workspace_dir/
git clone git@ops.gitlab.net:gitlab-com/gl-infra/deploy-tooling

# Set Python version
cd deploy-tooling/
PYTHON_VERSION=$(grep python .tool-versions | cut -d' ' -f2)

# Install Python
cd $HOME
mkdir tmp
cd tmp
wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz
tar zxf Python-$PYTHON_VERSION.tgz
cd Python-$PYTHON_VERSION/
./configure --prefix=$HOME/opt/python-$PYTHON_VERSION
make
make install

# Install venv
cd $workspace_dir/deploy-tooling
$HOME/opt/python-$PYTHON_VERSION/bin/python3 -m venv venv
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
ansible-galaxy collection install -r ansible-galaxy-requirements.yaml

# Ensure that you can generate an inventory.  One MUST have a working `gcloud`
ansible-inventory --graph
```

* One MUST have a working `gcloud` installation.
* If any changes are made to dependencies, python modules, or the Ansible
  version, a new CI image will need to be generated

### Docker image updates

This repository builds two custom Docker images: one for Python-based tooling,
and one for Ruby-based. Images are [built automatically in CI][ci-images] after
changes to their respective files, and tagged with the `$CI_COMMIT_SHORT_SHA`.

Jobs in [deployer][] utilizing this repository should use one of these custom
images as their base `image` CI configuration, along with the desired tag for
versioning.

To build images locally, clone the repository:

```sh
git clone git@ops.gitlab.net:gitlab-com/gl-infra/deploy-tooling.git
```

Then build and push with `make`:

```sh
export REGISTRY_ACCESS_TOKEN=<api token for ops.gitlab.net>
make build-docker-ci # build docker image
make push-docker-ci  # push to the ops registry
```

To build images on `deploy-tooling` MR pipelines, we need to have changes in files that trigger `image-update:*` jobs in
[`.gitlab-ci.yml`](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/673f460921d580c2791ed46c843821d418393d27/.gitlab-ci.yml#L73-99).

For example, a good line to modify for testing python image changes is to change `image:` in [`.gitlab-ci.yml`](./.gitlab-ci.yml) to point to a commit sha with the MR changes.

1. Push a commit with the actual changes to MR
1. Push another commit with a `.gitlab-ci.yml` change that points to the previous commit: `image: registry.ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/ci-image-py3:[first commit sha]`
1. That will trigger a new pipeline on the MR with `image-update:python` job.
1. Once that job finishes, clone [deployer] and create a branch for testing.
1. Update the `deploy-tooling` submodule, and `.gitlab-ci.yml` in deployer and push to the testing branch.

    ```sh
    # In deployer
    git submodule update --init --remote deploy-tooling
    cd deploy-tooling
    git checkout [latest commit of the deploy-tooling MR]
    cd ..
    # Modify .gitlab-ci.yml to use the image built from the `image-update:python` job in the previous step
    # image: registry.ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/ci-image-py3:[sha]
    # git add, commit, push,...
    ```

1. The deployer pipeline for that branch should now be using the image built from the new changes.

#### Python development

In this repository syntax is enforced with [python black](https://github.com/python/black),
which is a very opinionated code formatter. For local development it is
suggested to auto-format your code using `black` or run `black path/to/source.py`
before committing changes.

#### Suggested `git pre-commit` hook

```sh
#!/bin/sh
set -e

echo "Running yamllint"
yamllint --strict ./.*.yml ./*.yml ./*/**.yml

echo "Running ansible-lint"
ansible-lint ./*.yml

echo "Running flake8"
flake8
```

## One-Off Commands

The ```cmd.yml``` playbook and the playbooks in the ```cmds``` directory are
designed to allow simple playbooks to be run fleet wide.

Read more [here](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/blob/master/cmds/README.md).

[deployer]: https://ops.gitlab.net/gitlab-com/gl-infra/deployer
[ci-images]: https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/10cfe1d44357221ef9d4e2c2696fb6657468240d/.gitlab-ci.yml#L67-91
