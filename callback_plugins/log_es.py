# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

__metaclass__ = type

from os import environ

from datetime import datetime, timezone
from ansible.plugins.callback import CallbackBase
from ansible import context
from elasticsearch import Elasticsearch

DOCUMENTATION = """
    callback: log_es
    short_description: Posts summary stats to elasticsearch
    description:
      - This callback will post a log message to elasticsearch
    requirements:
      - whitelisting in configuration
    options:
      elasticsearch_host:
        description: URL for ElasticSearch
        required: True
        ini:
          - section: log_es
            key: elasticsearch_host
        env:
          - name: ELASTICSEARCH_HOST
      elasticsearch_password:
        description: Password for ElasticSearch
        required: True
        ini:
          - section: log_es
            key: elasticsearch_password
        env:
          - name: ELASTICSEARCH_PASSWORD
      elasticsearch_user:
        description: User for ElasticSearch
        required: True
        ini:
          - section: log_es
            key: elasticsearch_user
        env:
          - name: ELASTICSEARCH_USER
      elasticsearch_index:
        description: Index for ElasticSearch
        required: True
        ini:
          - section: log_es
            key: elasticsearch_index
        env:
          - name: ELASTICSEARCH_INDEX
      elasticsearch_port:
        description: User for ElasticSearch
        required: True
        ini:
          - section: log_es
            key: elasticsearch_port
        env:
          - name: ELASTICSEARCH_PORT
"""


def to_millis(dt):
    return int(dt.strftime("%s")) * 1000


class CallbackModule(CallbackBase):
    def v2_playbook_on_start(self, playbook):
        self.start_time = datetime.now()
        self.playbook = playbook

    def v2_playbook_on_stats(self, stats):
        end_time = datetime.now()

        msg = {}
        msg["playbook"] = self.playbook._file_name
        msg["duration_seconds"] = (end_time - self.start_time).seconds
        msg["env"] = {}
        for var in [
            "DEPLOY_VERSION",
            "CURRENT_DEPLOY_ENVIRONMENT",
            "STAGE",
            "CI_JOB_NAME",
            "CI_JOB_STAGE",
            "CI_JOB_MANUAL",
            "CI_JOB_URL",
            "CI_PIPELINE_URL",
            "CI_PROJECT_NAME",
            "RUN_MIGRATIONS",
            "CHECKMODE",
            "RUN_POST_DEPLOY_MIGRATIONS",
            "GITLAB_ROLES",
        ]:
            if environ.get(var):
                msg["env"][var] = environ.get(var)
        msg["check_mode"] = context.CLIARGS["check"]
        msg["hosts"] = {}
        msg["hosts"]["num"] = 0
        for host in list(stats.processed.keys()):
            msg["hosts"]["num"] += 1
            for k, v in stats.summarize(host).items():
                msg["hosts"][k] = v
        msg["timestamp"] = datetime.utcnow().replace(tzinfo=timezone.utc).isoformat()
        msg[
            "message"
        ] = "Completed ansible run for {job} in {env} " "which took {d}s".format(
            job=environ.get("CI_JOB_NAME", "unknown-job"),
            env=environ.get("CURRENT_DEPLOY_ENVIRONMENT", "unknown-env"),
            d=msg["duration_seconds"],
        )

        ###############################################
        # For compatibility with release-tools logging
        # This may change when https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/771
        # is resolved
        msg["name"] = environ.get("CI_PROJECT_NAME", "not-set")
        if environ.get("DEPLOY_VERSION"):
            msg["payload"] = {}
            msg["payload"]["name"] = environ["DEPLOY_VERSION"]
        ###############################################

        self.es_put(msg)

    def es_put(self, msg):
        es_host = self._plugin_options["elasticsearch_host"]
        es_user = self._plugin_options["elasticsearch_user"]
        es_pass = self._plugin_options["elasticsearch_password"]
        es_index = "{}-{}".format(
            self._plugin_options["elasticsearch_index"],
            datetime.today().strftime("%Y.%m.%d"),
        )
        es_port = self._plugin_options["elasticsearch_port"]

        es = Elasticsearch(
            [es_host], http_auth=(es_user, es_pass), scheme="https", port=es_port
        )

        try:
            es.index(index=es_index, body=msg)
        except Exception as error:
            self._display.display(
                f"ERROR posting log message to elasticsearch: {error}"
            )
