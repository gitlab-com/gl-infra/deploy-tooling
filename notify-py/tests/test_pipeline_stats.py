import os
import unittest
from unittest.mock import patch
from pipeline_stats import PipelineStats


class MockResponse:
    def __init__(self, status_code, json_resp, headers):
        self.status_code = status_code
        self.json_resp = json_resp
        self.headers = headers

    def json(self):
        return self.json_resp


def create_mocks(mock_session, mock_jobs_in_bridge_pipeline=True):
    jobs_resp = MockResponse(
        status_code=200,
        json_resp=[
            {
                "name": "dummy-env-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-14T10:01:00.000Z",
                "stage": "dummy-env-prepare-stage",
                "status": "success",
            },
            {
                "name": "pre-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-14T10:01:00.000Z",
                "stage": "pre-prepare-stage",
                "status": "success",
            },
            {
                "name": "pre-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-14T10:01:10.000Z",
                "stage": "pre-prepare-stage",
                "status": "failed",
            },
            {
                "name": "pre-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-14T10:01:20.000Z",
                "stage": "pre-prepare-stage",
                "status": "success",
            },
            {
                "name": "pre-finish-job1",
                "duration": 10,
                "started_at": "2019-06-14T10:02:00.000Z",
                "stage": "pre-finish-stage",
                "status": "success",
            },
            {
                "name": "pre-finish-job2",
                "duration": 20,
                "started_at": "2019-06-14T10:02:10.000Z",
                "stage": "pre-finish-stage",
                "status": "success",
            },
            {
                "name": "pre-finish-job3",
                "duration": 30,
                "started_at": "2019-06-14T10:02:20.000Z",
                "stage": "pre-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-cny-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-13T11:01:00.000Z",
                "stage": "gstg-cny-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-cny-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-13T11:01:10.000Z",
                "stage": "gstg-cny-prepare-stage",
                "status": "failure",
            },
            {
                "name": "gstg-cny-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-13T11:01:20.000Z",
                "stage": "gstg-cny-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-cny-finish-job1",
                "duration": 10,
                "started_at": "2019-06-13T11:02:00.000Z",
                "stage": "gstg-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-cny-finish-job2",
                "duration": 20,
                "started_at": "2019-06-13T11:02:10.000Z",
                "stage": "gstg-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-cny-finish-job3",
                "duration": 30,
                "started_at": "2019-06-13T11:02:20.000Z",
                "stage": "gstg-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-ref-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-13T11:01:00.000Z",
                "stage": "gstg-ref-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-ref-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-13T11:01:10.000Z",
                "stage": "gstg-ref-prepare-stage",
                "status": "failure",
            },
            {
                "name": "gstg-ref-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-13T11:01:20.000Z",
                "stage": "gstg-ref-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-ref-finish-job1",
                "duration": 10,
                "started_at": "2019-06-13T11:02:00.000Z",
                "stage": "gstg-ref-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-ref-finish-job2",
                "duration": 20,
                "started_at": "2019-06-13T11:02:10.000Z",
                "stage": "gstg-ref-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-ref-finish-job3",
                "duration": 30,
                "started_at": "2019-06-13T11:02:20.000Z",
                "stage": "gstg-ref-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-14T11:01:00.000Z",
                "stage": "gstg-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-14T11:01:10.000Z",
                "stage": "gstg-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-14T11:01:20.000Z",
                "stage": "gstg-prepare-stage",
                "status": "success",
            },
            {
                "name": "gstg-finish-job1",
                "duration": 10,
                "started_at": "2019-06-14T11:02:00.000Z",
                "stage": "gstg-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-finish-job2",
                "duration": 20,
                "started_at": "2019-06-14T11:02:10.000Z",
                "stage": "gstg-finish-stage",
                "status": "success",
            },
            {
                "name": "gstg-finish-job3",
                "duration": 30,
                "started_at": "2019-06-14T11:02:20.000Z",
                "stage": "gstg-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-cny-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-15T11:01:00.000Z",
                "stage": "gprd-cny-prepare-stage",
                "status": "success",
            },
            {
                "name": "gprd-cny-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-15T11:01:10.000Z",
                "stage": "gprd-cny-prepare-stage",
                "status": "failure",
            },
            {
                "name": "gprd-cny-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-15T11:01:20.000Z",
                "stage": "gprd-cny-prepare-stage",
                "status": "success",
            },
            {
                "name": "gprd-cny-finish-job1",
                "duration": 10,
                "started_at": "2019-06-15T11:02:00.000Z",
                "stage": "gprd-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-cny-finish-job2",
                "duration": 20,
                "started_at": "2019-06-15T11:02:10.000Z",
                "stage": "gprd-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-cny-finish-job3",
                "duration": 30,
                "started_at": "2019-06-15T11:02:20.000Z",
                "stage": "gprd-cny-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-prepare-job1",
                "duration": 10,
                "started_at": "2019-06-16T11:01:00.000Z",
                "stage": "gprd-prepare-stage",
                "status": "success",
            },
            {
                "name": "gprd-prepare-job2",
                "duration": 20,
                "started_at": "2019-06-16T11:01:10.000Z",
                "stage": "gprd-prepare-stage",
                "status": "success",
            },
            {
                "name": "gprd-prepare-job3",
                "duration": 30,
                "started_at": "2019-06-16T11:01:20.000Z",
                "stage": "gprd-prepare-stage",
                "status": "success",
            },
            {
                "name": "gprd-finish-job1",
                "duration": 10,
                "started_at": "2019-06-16T11:02:00.000Z",
                "stage": "gprd-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-finish-job2",
                "duration": 20,
                "started_at": "2019-06-16T11:02:10.000Z",
                "stage": "gprd-finish-stage",
                "status": "success",
            },
            {
                "name": "gprd-finish-job3",
                "duration": 30,
                "started_at": "2019-06-16T11:02:20.000Z",
                "stage": "gprd-finish-stage",
                "status": "success",
            },
        ],
        headers={"X-Next-Page": ""},
    )

    if mock_jobs_in_bridge_pipeline:
        bridges_resp = MockResponse(
            status_code=200,
            json_resp=[
                {
                    "status": "success",
                    "stage": "release-qa",
                    "name": "release-gitlab-qa-smoke",
                },
                {
                    "status": "failed",
                    "stage": "dummy-qa",
                    "name": "dummy_bridge-gitlab-qa-smoke",
                },
            ],
            headers={"X-Next-Page": ""},
        )
    else:
        bridges_resp = MockResponse(
            status_code=200,
            json_resp=[],
            headers={"X-Next-Page": ""},
        )

    pipeline_resp = MockResponse(
        status_code=200,
        json_resp={
            "status": "failed",
            "web_url": "https://ops.gitlab.net/gitlab-com/gl-infra/deployer/pipelines/58485",
            "created_at": "2019-06-14T09:33:55.862Z",
            "updated_at": "2019-06-14T15:15:55.922Z",
            "started_at": "2019-06-14T09:33:58.435Z",
            "finished_at": "2019-06-14T15:15:55.905Z",
        },
        headers={"X-Next-Page": ""},
    )

    mock_session.return_value.get.side_effect = [pipeline_resp, jobs_resp, bridges_resp]


class TestPipelineStats(unittest.TestCase):
    @patch("requests.Session")
    @patch.dict(os.environ, {"CI_PIPELINE_ID": "1", "CI_PROJECT_ID": "1"})
    def env_setup(self, env, mock_session):
        create_mocks(mock_session)
        return PipelineStats("some-token", env)

    @patch("requests.Session")
    @patch.dict(os.environ, {"CI_PIPELINE_ID": "1", "CI_PROJECT_ID": "1"})
    def env_setup_no_jobs_in_bridge_pipeline(self, env, mock_session):
        create_mocks(mock_session, False)
        return PipelineStats("some-token", env)

    def test_status(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertTrue(pipeline_stats.status["status"] == "failed")

    def test_jobs_no_env(self):
        pipeline_stats = self.env_setup("does-not-exist")
        jobs = []
        self.assertEqual(pipeline_stats.jobs, jobs)

    def test_jobs_dummy_env(self):
        pipeline_stats = self.env_setup("dummy")
        jobs = [
            {
                "duration": 10,
                "name": "dummy-env-prepare-job1",
                "stage": "dummy-env-prepare-stage",
                "started_at": "2019-06-14T10:01:00.000Z",
                "status": "success",
            }
        ]
        self.assertEqual(pipeline_stats.jobs, jobs)

    def test_pipeline_url(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertEqual(
            pipeline_stats.pipeline_url(),
            "https://ops.gitlab.net/gitlab-com/gl-infra/deployer/pipelines/58485",
        )

    def test_stages_duration(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertEqual(pipeline_stats.stages_duration(), 60)

    def test_first_job_start_time_gstg_cny(self):
        pipeline_stats = self.env_setup("gstg-cny")
        self.assertEqual(
            str(pipeline_stats.first_job_start_time()), "2019-06-13 11:01:00+00:00"
        )

    def test_first_job_start_time_gstg_ref(self):
        pipeline_stats = self.env_setup("gstg-ref")
        self.assertEqual(
            str(pipeline_stats.first_job_start_time()), "2019-06-13 11:01:00+00:00"
        )

    def test_first_job_start_time_gstg(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertEqual(
            str(pipeline_stats.first_job_start_time()), "2019-06-14 11:01:00+00:00"
        )

    def test_first_job_start_time_gprd_cny(self):
        pipeline_stats = self.env_setup("gprd-cny")
        self.assertEqual(
            str(pipeline_stats.first_job_start_time()), "2019-06-15 11:01:00+00:00"
        )

    def test_first_job_start_time_gprd(self):
        pipeline_stats = self.env_setup("gprd")
        self.assertEqual(
            str(pipeline_stats.first_job_start_time()), "2019-06-16 11:01:00+00:00"
        )

    def test_for_job_failure_success(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertFalse(pipeline_stats.has_job_failure())

    def test_for_job_failure_failed(self):
        pipeline_stats = self.env_setup("pre")
        self.assertTrue(pipeline_stats.has_job_failure())

    def test_pipeline_start_time(self):
        pipeline_stats = self.env_setup("gstg")
        self.assertEqual(
            str(pipeline_stats.pipeline_start_time()),
            "2019-06-14 09:33:58.435000+00:00",
        )

    def test_job_starts_with_prefix(self):
        job_env_matchers = [
            {"job": "gprd-prepare", "env": "gprd", "is_match": True},
            {"job": "gprd-prepare", "env": "gstg", "is_match": False},
            {"job": "gprd-cny-prepare", "env": "gprd-cny", "is_match": True},
            {"job": "gprd-cny-prepare", "env": "gprd", "is_match": False},
            {"job": "gprd-cny-some-long-job-name", "env": "gprd-cny", "is_match": True},
            {"job": "invalidenv-prepare", "env": "gprd", "is_match": False},
        ]
        for job in job_env_matchers:
            self.assertEqual(
                PipelineStats.job_starts_with_prefix(job["job"], job["env"]),
                job["is_match"],
            )

    def test_bridges_exist(self):
        pipeline_stats = self.env_setup("release")
        self.assertEqual(len(pipeline_stats.bridge_pipelines), 1)

    def test_no_bridges(self):
        pipeline_stats = self.env_setup("does-not-exist")
        pipelines = []
        self.assertEqual(pipeline_stats.bridge_pipelines, pipelines)

    def test_no_jobs_in_bridge_pipeline(self):
        pipeline_stats = self.env_setup_no_jobs_in_bridge_pipeline("release")
        self.assertEqual(len(pipeline_stats.bridge_pipelines), 0)

    def test_bridges_success(self):
        pipeline_stats = self.env_setup("release")
        self.assertFalse(pipeline_stats.has_job_failure())

    def test_bridges_not_success(self):
        pipeline_stats = self.env_setup("dummy_bridge")
        bridge_pipelines = [
            {
                "status": "failed",
                "stage": "dummy-qa",
                "name": "dummy_bridge-gitlab-qa-smoke",
            }
        ]
        self.assertEqual(pipeline_stats.bridge_pipelines, bridge_pipelines)
        self.assertTrue(pipeline_stats.has_job_failure())
