import unittest
from unittest.mock import patch
from notify import Notify
from dateutil import parser
from freezegun import freeze_time
import os


class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code


class TestNotify(unittest.TestCase):
    @patch.dict(os.environ, {"GRAFANA_API_KEY": "fake-key"})
    @freeze_time("2019-01-04 01:00:00")
    @patch("requests.Session")
    @patch("pipeline_stats.PipelineStats")
    @patch("slack.WebClient")
    def setUp(self, mock_slack_client, mock_pipeline_stats, mock_session):
        mock_session.return_value.post.return_value = MockResponse(status_code=200)
        self.mock_slack_client = mock_slack_client.return_value
        pipeline = mock_pipeline_stats()
        pipeline.has_job_failure.return_value = True
        pipeline.pipeline_url.return_value = "https://example.com"
        pipeline.first_job_start_time.return_value = parser.parse(
            "2019-01-04T00:00:00.00Z"
        )

        pipeline.stages_duration.return_value = 50
        self.notify = Notify(
            "some-user",
            "some-project",
            pipeline,
            "gstg",
            "13.0.202005130510-023ea2fbc0e.36302f92275",
            "some-token",
        )

    def test_add_emojis(self):
        self.assertEqual(
            self.notify.add_emojis(env="gstg", success=True, start=True),
            ":building_construction: :ci_running:",
        )
        self.assertEqual(
            self.notify.add_emojis(env="gstg", success=True, start=False),
            ":building_construction: :ci_passing:",
        )
        self.assertEqual(
            self.notify.add_emojis(env="gstg", success=False, start=True),
            ":building_construction: :ci_running:",
        )
        self.assertEqual(
            self.notify.add_emojis(env="gstg", success=False, start=False),
            ":building_construction: :ci_failing:",
        )
        self.assertEqual(
            self.notify.add_emojis(env="dr", success=True, start=False),
            ":umbrella_with_rain_drops: :ci_passing:",
        )
        self.assertEqual(
            self.notify.add_emojis(env="invalid", success=True, start=False),
            ":question: :ci_passing:",
        )

    def test_gen_grafana_text(self):
        self.assertEqual(
            self.notify.gen_grafana_text(success=True, start=True),
            "some-user is starting a some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg",
        )
        self.assertEqual(
            self.notify.gen_grafana_text(success=True, start=False),
            "some-user finished a some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg which had end-to-end wall clock duration of an hour (and sum of pipeline stage durations was 50 seconds)",
        )
        self.assertEqual(
            self.notify.gen_grafana_text(success=False, start=False),
            "some-user finished a failed some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg which had end-to-end wall clock duration of an hour (and sum of pipeline stage durations was 50 seconds)",
        )

    @patch.dict(
        os.environ,
        {
            "CI_PIPELINE_URL": "https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/2866045"
        },
    )
    def test_gen_grafana_text_with_pipeline_url(self):
        self.assertEqual(
            self.notify.gen_grafana_text(success=True, start=True),
            'some-user is starting a <a href="https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/2866045">some-project pipeline</a> of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg',
        )

    @patch.dict(os.environ, {"DEPLOY_ROLLBACK": "true"})
    @patch("notify.Notify.grafana_annotate")
    @patch("notify.Notify.slack_notify")
    def test_rollback(self, mock_slack_notify, mock_grafana_annotate):
        self.notify.start()
        mock_grafana_annotate.assert_called_with(
            {
                "time": 1546563600000,
                "text": "some-user is starting a some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg",
                "tags": [
                    "deploy",
                    "some-user",
                    "gstg",
                    "rollback",
                    "13.0.202005130510-023ea2fbc0e.36302f92275",
                ],
            }
        )
        mock_slack_notify.assert_called_with(
            [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":building_construction: :ci_running: *gstg* <https://example.com|started> a *rollback* to `13.0.202005130510-023ea2fbc0e.36302f92275`",
                    },
                },
                {
                    "type": "context",
                    "elements": [
                        {"type": "mrkdwn", "text": ":clock1: 2019-01-04 01:00 UTC"},
                        {
                            "type": "mrkdwn",
                            "text": ":sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/023ea2fbc0e/|View Sentry>",
                        },
                    ],
                },
            ],
            channels=["#announcements"],
        )

    @patch("notify.Notify.grafana_annotate")
    @patch("notify.Notify.slack_notify")
    def test_start(self, mock_slack_notify, mock_grafana_annotate):
        self.notify.start()
        mock_grafana_annotate.assert_called_with(
            {
                "time": 1546563600000,
                "text": "some-user is starting a some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg",
                "tags": [
                    "deploy",
                    "some-user",
                    "gstg",
                    "13.0.202005130510-023ea2fbc0e.36302f92275",
                ],
            }
        )
        mock_slack_notify.assert_called_with(
            [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":building_construction: :ci_running: *gstg* <https://example.com|started> `13.0.202005130510-023ea2fbc0e.36302f92275`",
                    },
                },
                {
                    "type": "context",
                    "elements": [
                        {"type": "mrkdwn", "text": ":clock1: 2019-01-04 01:00 UTC"},
                        {
                            "type": "mrkdwn",
                            "text": ":sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/023ea2fbc0e/|View Sentry>",
                        },
                    ],
                },
            ],
            channels=["#announcements"],
        )

    @patch("notify.Notify.grafana_annotate")
    @patch("notify.Notify.slack_notify")
    def test_finish_success(self, mock_slack_notify, mock_grafana_annotate):
        self.notify.finish_success()
        mock_grafana_annotate.assert_called_with(
            {
                "time": 1546563550000,
                "timeEnd": 1546563600000,
                "isRegion": True,
                "text": "some-user finished a some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg which had end-to-end wall clock duration of an hour (and sum of pipeline stage durations was 50 seconds)",
                "tags": [
                    "deploy",
                    "some-user",
                    "gstg",
                    "13.0.202005130510-023ea2fbc0e.36302f92275",
                ],
            }
        )
        mock_slack_notify.assert_called_with(
            [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":building_construction: :ci_passing: *gstg* <https://example.com|finished> `13.0.202005130510-023ea2fbc0e.36302f92275`",
                    },
                },
                {
                    "type": "context",
                    "elements": [
                        {"type": "mrkdwn", "text": ":clock1: 2019-01-04 01:00 UTC"},
                        {
                            "type": "mrkdwn",
                            "text": ":sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/023ea2fbc0e/|View Sentry>",
                        },
                        {"type": "mrkdwn", "text": ":timer_clock: an hour"},
                    ],
                },
            ],
            channels=["#announcements"],
        )

    @patch("notify.Notify.grafana_annotate")
    @patch("notify.Notify.slack_notify")
    def test_finish_failure(self, mock_slack_notify, mock_grafana_annotate):
        self.notify.finish_failure()
        mock_grafana_annotate.assert_called_with(
            {
                "time": 1546563600000,
                "text": "some-user finished a failed some-project pipeline of 13.0.202005130510-023ea2fbc0e.36302f92275 on gstg which had end-to-end "
                + "wall clock duration of an hour (and sum of pipeline stage durations was 50 seconds)",
                "tags": [
                    "deploy",
                    "some-user",
                    "gstg",
                    "13.0.202005130510-023ea2fbc0e.36302f92275",
                ],
            }
        )
        mock_slack_notify.assert_called_with(
            [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": ":building_construction: :ci_failing: *gstg* *<https://example.com|failed>* `13.0.202005130510-023ea2fbc0e.36302f92275`",
                    },
                },
                {
                    "type": "context",
                    "elements": [
                        {"type": "mrkdwn", "text": ":clock1: 2019-01-04 01:00 UTC"},
                        {
                            "type": "mrkdwn",
                            "text": ":sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/023ea2fbc0e/|View Sentry>",
                        },
                        {"type": "mrkdwn", "text": ":timer_clock: an hour"},
                        {
                            "type": "mrkdwn",
                            "text": ":book: <https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/failures.md|View failure runbook>",
                        },
                    ],
                },
            ],
            channels=["#announcements"],
        )

    def test_slack_notify(self):
        blocks = [{"text": {"text": "some notification text"}}]
        self.notify.slack_notify(blocks, ["#some-channel"])
        self.mock_slack_client.chat_postMessage.assert_called_with(
            channel="#some-channel", text="some notification text", blocks=blocks
        )

    @patch.dict(os.environ, {"CHECKMODE": "true"})
    def test_slack_notify_checkmode(self):
        print("mock_name:", self.mock_slack_client)
        blocks = [{"text": {"text": "some notification text"}}]
        self.notify.slack_notify(blocks, ["#some-channel"])
        self.mock_slack_client.chat_postMessage.assert_called_with(
            channel="#announcements-test", text="some notification text", blocks=blocks
        )
