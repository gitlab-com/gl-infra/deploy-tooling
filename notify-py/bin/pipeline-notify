#!/usr/bin/env python

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "lib"))
sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "notify-py", "lib")
)
from pipeline_stats import PipelineStats
from notify import Notify


def parse_args():
    parser = argparse.ArgumentParser(
        description="Sends end of deploy pipeline notifications"
    )
    parser.add_argument(
        "env_stage",
        help="Name of the environment with the optional stage ending, example: gprd-cny, gprd, gstg-cny, gstg-ref, gstg, pre",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--start",
        action="store_true",
        default=False,
        help="Send a success start notification",
    )
    group.add_argument(
        "--finish-success",
        action="store_true",
        default=False,
        help="Send a success finish notification",
    )
    group.add_argument(
        "--finish-failure",
        action="store_true",
        default=False,
        help="Send a failure finish notification",
    )
    return parser.parse_args()


def main():

    args = parse_args()

    username = (
        os.environ.get("DEPLOY_USER")
        or os.environ.get("GITLAB_USER_NAME")
        or os.environ.get("USER")
        or "unknown"
    )

    project_name = os.environ.get("CI_PROJECT_NAME", "unknown-project")
    bot_token = os.environ.get("SLACK_DEPLOYER_TOKEN")
    version = os.environ.get("DEPLOY_VERSION")
    api_token = os.environ["OPS_PIPELINE_API_TOKEN"]
    filter_job_prefix = args.env_stage

    pipeline = PipelineStats(api_token, filter_job_prefix)
    notify = Notify(
        username, project_name, pipeline, args.env_stage, version, bot_token
    )
    if args.start:
        notify.start()
    elif args.finish_success:
        notify.finish_success()
    elif args.finish_failure:
        notify.finish_failure()


if __name__ == "__main__":
    main()
