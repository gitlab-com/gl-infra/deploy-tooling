import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


class RetrySession:
    @staticmethod
    def create():
        session = requests.Session()
        retries = Retry(
            total=5, backoff_factor=1, status_forcelist=[500, 502, 503, 504]
        )
        session.mount("https://", HTTPAdapter(max_retries=retries))
        return session
