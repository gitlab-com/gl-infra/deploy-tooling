import os
from itertools import groupby
from dateutil import parser
from requests.adapters import RetryError
from retry_session import RetrySession
import sys
import re


class PipelineStats:
    """
    Collects information about a pipeline in the context of
    deployments
    """

    def __init__(self, api_token, filter_job_prefix=None):
        self.api_token = api_token
        self.filter_job_prefix = filter_job_prefix
        self.pipeline_id = os.environ["CI_PIPELINE_ID"]
        self.project_id = os.environ["CI_PROJECT_ID"]
        self.status = self._status()
        self.jobs = self._jobs()
        self.bridge_pipelines = self._bridge_pipelines()

    def pipeline_url(self):
        return self.status["web_url"]

    def stages_duration(self):
        # Returns the total number of seconds
        # spent in pipeline stages by summing
        # the jobs in each stage with the longest
        # duration
        stages_duration = 0
        for stage in groupby(self.jobs, lambda x: x["stage"]):
            durations = [item["duration"] for item in stage[1] if item["duration"]]
            if durations:
                stages_duration += max(durations)
        return stages_duration

    def first_job_start_time(self):
        # Returns the time of the first job
        first_start_time = min(
            [job["started_at"] for job in self.jobs if job["started_at"] is not None]
        )
        return parser.parse(first_start_time)

    def pipeline_start_time(self):
        return parser.parse(self.status["started_at"])

    def has_job_failure(self):
        return any(
            [job["status"] == "failed" for job in (self.jobs + self.bridge_pipelines)]
        )

    def _status(self):
        session = RetrySession.create()
        url = f"https://ops.gitlab.net/api/v4/projects/{self.project_id}/pipelines/{self.pipeline_id}"
        try:
            resp = session.get(url, headers={"Private-Token": self.api_token})
        except RetryError as e:
            print(f"Unable to fetch the pipeline url {url}: {e}")
            sys.exit(1)
        if resp.status_code != 200:
            print(f"{resp.status_code} returned for {url}, is it a valid pipeline?")
            sys.exit(1)
        return resp.json()

    def _jobs(self):
        # Fetches the list of pipeline jobs using
        # the GitLab API, applies a text filter
        # over the job name if it is provided

        url = f"https://ops.gitlab.net/api/v4/projects/{self.project_id}/pipelines/{self.pipeline_id}/jobs"
        return self._get_failed_jobs(url)

    def _bridge_pipelines(self):
        # Fetches the status of bridge pipelines using
        # the GitLab API, applies a text filter
        # over the job name if it is provided

        url = f"https://ops.gitlab.net/api/v4/projects/{self.project_id}/pipelines/{self.pipeline_id}/bridges"
        return self._get_failed_jobs(url)

    def _get_failed_jobs(self, url):
        # Fetches the status of jobs or bridge jobs using
        # the GitLab API URL, and applies a text filter
        # over the job name if it is provided

        session = RetrySession.create()
        jobs = []
        page = "1"
        while True:
            try:
                resp = session.get(
                    url,
                    headers={"Private-Token": self.api_token},
                    params={"page": page},
                )
                jobs += resp.json()
            except RetryError as e:
                print(f"ERROR: Unable to fetch the pipeline url {url}: {e}")
                sys.exit(1)
            if resp.status_code != 200:
                print(f"{resp.status_code} returned for {url}, is it a valid pipeline?")
                sys.exit(1)
            next_page = resp.headers.get("X-Next-Page")
            if next_page and next_page != "":
                page = next_page
                print(f"Fetching page #{page}")
            else:
                break

        return (
            [
                job
                for job in jobs
                if self.job_starts_with_prefix(job["name"], self.filter_job_prefix)
            ]
            if self.filter_job_prefix
            else jobs
        )

    @staticmethod
    def job_starts_with_prefix(job_name, filter_job_prefix):
        # There is currently no way to derive an environment name from a job,
        # tracked in https://gitlab.com/gitlab-org/gitlab/-/issues/30468

        # The hardcoded strings "-cny" and "-ref" is necessary here so we can
        # differentiate between gprd-cny-some-job-name and gprd-some-job-name
        # since both of these start with 'gprd'
        job_env_from_prefix, number_subs = re.subn(
            "^([^ -]+?(?:-cny|-ref)?)-.*", r"\1", job_name
        )
        if number_subs != 1:
            print(
                f"Warning: Unable to find a job {job_name} with prefix {filter_job_prefix}"
            )
            return False
        return job_env_from_prefix == filter_job_prefix
