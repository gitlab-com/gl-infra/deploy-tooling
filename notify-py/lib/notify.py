import json
import humanize
from datetime import datetime, timezone
from retry_session import RetrySession
from requests.adapters import RetryError
import sys
import re
import os
import yaml
import slack
from os.path import join, dirname, abspath
from distutils.util import strtobool


class Notify:
    def __init__(self, username, project_name, pipeline, env, version, bot_token):
        self.username = username
        self.project_name = project_name
        self.pipeline = pipeline
        self.env = env
        self.version = version
        self.dt_now = datetime.now(timezone.utc)
        self.bot_token = bot_token
        notify_cfg_file = join(
            abspath(dirname(__file__)), "..", "..", "cfg", "pipeline-notify.yaml"
        )
        self.cfg = yaml.safe_load(open(notify_cfg_file).read())
        self.slack_client = slack.WebClient(token=bot_token)

    def start(self):
        print("Issuing start notification")
        self.grafana_annotate(
            {
                "time": Notify.to_millis(self.dt_now),
                "text": self.gen_grafana_text(success=True, start=True),
                "tags": self.grafana_tags(),
            }
        )
        self.slack_notify(
            self.gen_slack_block(success=True, start=True),
            channels=self.cfg["channel_notifications"]["pipeline"]["finish_failure"],
        )

    def finish_success(self):
        print("Issuing notification for success")
        grafana_data = {
            "time": Notify.to_millis(self.dt_now)
            - int(self.pipeline.stages_duration() * 1000),
            "timeEnd": Notify.to_millis(self.dt_now),
            "isRegion": True,
            "text": self.gen_grafana_text(success=True, start=False),
            "tags": self.grafana_tags(),
        }

        self.grafana_annotate(grafana_data)
        self.slack_notify(
            self.gen_slack_block(success=True, start=False),
            channels=self.cfg["channel_notifications"]["pipeline"]["finish_success"],
        )

    def finish_failure(self):
        print("Issuing notification for failure")
        if not self.pipeline.has_job_failure():
            print(
                "Not issuing failure notification because there are no jobs marked as failed in this pipeline"
            )
            return
        grafana_data = {
            "time": Notify.to_millis(self.dt_now),
            "text": self.gen_grafana_text(success=False, start=False),
            "tags": self.grafana_tags(),
        }

        self.grafana_annotate(grafana_data)
        self.slack_notify(
            self.gen_slack_block(success=False, start=False),
            channels=self.cfg["channel_notifications"]["pipeline"]["finish_failure"],
        )

    def grafana_tags(self):
        tags = ["deploy", self.username, self.env]

        if os.getenv("DEPLOY_ROLLBACK"):
            tags.append("rollback")

        if self.version:
            tags.append(self.version)

        return tags

    def gen_grafana_text(self, success, start):
        print(f"Generating Grafana notification for success={success} start={start}")

        version_text = f"of {self.version}" if self.version else ""
        project_name_text = f"{self.project_name} pipeline"
        stages_duration = humanize.naturaldelta(self.pipeline.stages_duration())
        wall_delta = self.dt_now - self.pipeline.first_job_start_time()

        pipeline_url = os.environ.get("CI_PIPELINE_URL")
        if pipeline_url is not None:
            project_name_text = f'<a href="{pipeline_url}">{project_name_text}</a>'

        notif = []
        notif.append(self.username)
        notif.append("is starting a" if start else "finished a")
        if not success:
            notif.append("failed")
        notif.append(f"{project_name_text}")
        notif.append(f"{version_text}")
        notif.append(f"on {self.env}")
        if not start:
            # Always show wall clock duration (start of first job to end of last job).
            wall_duration = humanize.naturaldelta(wall_delta)
            notif.append(f"which had end-to-end wall clock duration of {wall_duration}")

            # Also display sum of stage durations if the difference is greater than 2 minutes.
            # Stage duration can be larger than wall clock duration if some stages run concurrently.
            delta = abs(wall_delta.seconds - self.pipeline.stages_duration())
            print(f"Delta between wall time and pipeline time is: {delta}")
            if delta > 120:
                notif.append(
                    f"(and sum of pipeline stage durations was {stages_duration})"
                )

        return " ".join(notif)

    def gen_slack_block(self, success, start):
        print(f"Generating Slack block for success={success} start={start}")

        pipeline_url = self.pipeline.pipeline_url()

        text = []
        text.append(self.add_emojis(self.env, success, start))
        text.append(f"*{self.env}*")

        if start:
            text.append(f"<{pipeline_url}|started>")
        elif not success:
            text.append(f"*<{pipeline_url}|failed>*")
        else:
            text.append(f"<{pipeline_url}|finished>")

        if os.getenv("DEPLOY_ROLLBACK"):
            text.append("a *rollback* to")

        if self.version:
            text.append(f"`{self.version}`")
        else:
            text.append(f"{os.getenv('CI_PROJECT_NAME', 'unknown')} pipeline")

        context = []

        # Show UTC time as a debugging aid
        context.append(f":clock1: {self.dt_now.strftime('%Y-%m-%d %H:%M')} UTC")

        if self.version_sha():
            context.append(
                f":sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/{self.version_sha()}/|View Sentry>"
            )

        if not start:
            # Always show wall clock duration (start of first job to end of last job).
            wall_delta = self.dt_now - self.pipeline.first_job_start_time()
            wall_duration = humanize.naturaldelta(wall_delta)

            context.append(f":timer_clock: {wall_duration}")

        if not success:
            context.append(
                ":book: <https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/failures.md|View failure runbook>"
            )

        block = [
            {"type": "section", "text": {"type": "mrkdwn", "text": " ".join(text)}}
        ]

        if context:
            block.append(
                {
                    "type": "context",
                    "elements": list(
                        {"type": "mrkdwn", "text": text} for text in context
                    ),
                }
            )

        return block

    @staticmethod
    def checkmode():
        return strtobool(os.getenv("CHECKMODE", "false"))

    @staticmethod
    def to_millis(dt):
        return int(dt.timestamp()) * 1000

    def add_emojis(self, env, success, start):
        slack_emojis = self.cfg["slack_emojis"]
        emojis = []
        if Notify.checkmode():
            c_emoji = slack_emojis["checkmode"]
            emojis.append(f"{c_emoji} *check mode* {c_emoji}")
        emoji = slack_emojis["environments"].get(env)
        emojis.append(emoji if emoji else slack_emojis["unknown"])

        if start:
            emojis.append(slack_emojis["start"])
        else:
            if success:
                emojis.append(slack_emojis["success"])
            else:
                emojis.append(slack_emojis["warning"])
        return " ".join(emojis)

    def grafana_annotate(self, grafana_data):
        print(f"Annotating grafana with {grafana_data}")
        if Notify.checkmode() or not os.environ.get("GRAFANA_API_KEY"):
            print("Running in checkmode, skipping grafana annotations")
            return

        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(os.environ.get("GRAFANA_API_KEY")),
        }
        grafana_url = os.environ.get("GRAFANA_URL")
        session = RetrySession.create()
        try:
            session.post(grafana_url, data=json.dumps(grafana_data), headers=headers)
        except RetryError as e:
            print(f"Unable to post stats to {grafana_url}: {e}")
            sys.exit(1)

    def slack_notify(self, blocks, channels):
        text = blocks[0]["text"]["text"]

        # NOTE: Also checking SKIP_PIPELINE_NOTIFY due to partial release-tools implementation
        # See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2117
        if os.getenv("NO_NOTIFY") or os.getenv("SKIP_PIPELINE_NOTIFY") == "true":
            print("Skipping notification to Slack because NO_NOTIFY is set")
            return
        if Notify.checkmode():
            channels = self.cfg["channel_notifications"]["pipeline"]["testing"]

        print(f"Sending slack notification to channels: {channels}")
        print(json.dumps(blocks))

        for channel in channels:
            resp = self.slack_client.chat_postMessage(
                channel=channel, text=text, blocks=blocks
            )
            assert resp["ok"]

    def version_sha(self):
        """
        If the version looks like an auto-deploy tag,
        extract the sha from the version
        """

        if not self.version:
            return None

        found = re.search(r"\d+\.\d+\.\d+[-+]([^ .]+?)\.[^ ]+", self.version)

        if not found:
            return None

        return found.group(1)
