#!/bin/bash

set -e

dir="$(
  cd "$(dirname "${0}")"
  pwd
)"

ansible_extra_opts=()

if [[ "$CI" ]]; then
  mkdir -p /root/.ssh
  chmod 700 /root/.ssh

  echo "$SSH_PUBLIC_KEY" >/root/.ssh/id_rsa.pub
  echo "$SSH_PRIVATE_KEY" >/root/.ssh/id_rsa

  chmod 644 /root/.ssh/id_rsa.pub
  chmod 600 /root/.ssh/id_rsa

  ansible_extra_opts+=("--user=$SSH_USERNAME" "--key-file=/root/.ssh/id_rsa")
fi

export ANSIBLE_FORCE_COLOR=true
export ANSIBLE_HOST_KEY_CHECKING=False

dir=$(dirname "$(dirname "$(realpath "$0")")")
export ANSIBLE_CONFIG="$dir/ansible.cfg"
# Work-around for auto-deploy branches
export DEPLOY_VERSION="${DEPLOY_VERSION/-rfbranch/+rfbranch}"

if [[ "$CHECKMODE" == "true" ]]; then
  ansible_extra_opts+=("--check")
fi

if [[ "$CI_COMMIT_REF_NAME" != "master" ]]; then
  if [[ -n "$ANSIBLE_EXTRA_VERBOSE" ]]; then
    ansible_extra_opts+=("-vvv")
  else
    ansible_extra_opts+=("-v")
  fi
fi

if [[ -n "${ANSIBLE_NUM_FORKS}" ]]; then
  ansible_extra_opts+=("--forks" "${ANSIBLE_NUM_FORKS}")
fi

cat <<RUN_INFO
=== RUN INFO ===
Roles: ${GITLAB_ROLES:-not set}
Environment: ${CURRENT_DEPLOY_ENVIRONMENT:-not set}
Stage: ${STAGE:-not set}
Deploy Version: ${DEPLOY_VERSION:-not set}
Run Migrations: ${RUN_MIGRATIONS:-not set}
Skip tags: ${ANSIBLE_SKIP_TAGS:-not set}
Check Mode: ${CHECKMODE:-not set}
Ignore errors: ${PRECHECK_IGNORE_ERRORS:-not set}
Post deploy migrations: ${RUN_POST_DEPLOY_MIGRATIONS:-not set}
Ansible Extra Opts: ${ansible_extra_opts[*]}
Ansible Forks: ${ANSIBLE_NUM_FORKS:-using default}
RUN_INFO

ansible-playbook "${ansible_extra_opts[@]}" "$@"
