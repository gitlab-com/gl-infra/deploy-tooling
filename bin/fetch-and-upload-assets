#!/bin/bash

##############################
# fetch-and-upload-assets
#
# Fetches assets from the assets registry image
# and uploads them to the assets bucket.
#
# If the registry image is not available, it will instead
# attempt to extract assets from a package.
#
# This script requires a service account that has
# write access to the corresponding asset bucket
#
#
# To test or manually upload assets using the script:
#
#   Set the following vars in env:
#
#     CURRENT_DEPLOY_ENVIRONMENT
#     DEPLOY_VERSION
#     DEV_ASSETS_DEPLOY_TOKEN
#     DEV_ASSETS_DEPLOY_TOKEN_USER
#     DEV_ASSETS_SERVICE_ACCOUNT_KEY
#     PACKAGECLOUD_TOKEN
#
#   Run locally (requires gnu version of jq,ar,wget,tar):
#
#     bash ./bin/fetch-and-upload-assets
#
#   For testing:
#
#     docker run  -v /var/run/docker.sock:/var/run/docker.sock \
#       --privileged=true -it --entrypoint=/bin/sh docker:stable
#     docker cp fetch-and-upload-assets <id>:/fetch-and-upload-assets
#     wget https://storage.googleapis.com/pub/gsutil.tar.gz
#     tar zxvf gsutil.tar.gz
#     export PATH="$PATH:${CI_PROJECT_DIR}/gsutil"
#     apk add bash binutils curl jq tar python py-crcmod
#     bash /fetch-and-upload-assets
#
##############################
set -euf -o pipefail

debug_seconds() {
  [[ $ASSETS_DEBUG == true ]] && echo "DEBUG: seconds since script start: $SECONDS"
}

# Expand a shortened gitlab-rails SHA ($1) to its full ID
expand_rails_sha() {
  local api_url
  local full_sha

  # gitlab/gitlab-ee dev mirror
  api_url="https://dev.gitlab.org/api/v4/projects/194/repository/commits/$1"

  set +x
  full_sha=$(curl -s --header "PRIVATE-TOKEN: $DEV_API_TOKEN" "$api_url" | jq -r '.id')
  set -x

  echo "$full_sha"
}

fetch_from_pkg() {
    set +x
    echo "Downloading $DEPLOY_VERSION from package cloud"
    # For asset collection use the ubuntu/focal package
    # The assets for amd64 and arm64 are identical, so it shouldn't
    # matter which one we pick. But to be safe, let's use amd64 to match
    # the Kubernetes deployment.
    download_url=$(wget -q -O - "https://$PACKAGECLOUD_TOKEN:@packages.gitlab.com/api/v1/repos/gitlab/pre-release/search?q=$DEPLOY_VERSION" | jq -r '.[] | select(.distro_version=="ubuntu/focal") | .download_url' | grep amd64)
    if [[ -z "$download_url" ]]; then
        echo "Unable to find $DEPLOY_VERSION on package cloud"
        exit 1
    fi
    wget -q -O "${DEPLOY_VERSION}.deb" "$download_url"
    debug_seconds

    echo "Extracting assets from ${DEPLOY_VERSION}.deb"
    set -x
    ar xv "${DEPLOY_VERSION}.deb"
    # This extracts the assets directory from the debian package
    # and strips everything up until the assets/ dir, resulting
    # in "$tmpdir/assets/..."
    tar -xf data.tar.xz --strip-components=7 -C "$tmpdir" ./opt/gitlab/embedded/service/gitlab-rails/public/assets
    debug_seconds
}

fetch_from_registry() {
    auto_deploy_pattern="^[0-9]+\.[0-9]+\.[0-9]+-([A-Fa-f0-9]{8,11})\.[A-Fa-f0-9]{8,11}$"
    if [[ $DEPLOY_VERSION =~ $auto_deploy_pattern ]]; then
        # Extract and expand the gitlab-rails SHA
        rails_short_id=$(echo "$DEPLOY_VERSION" | sed -E "s/$auto_deploy_pattern/\1/")
        assets_version=$(expand_rails_sha "$rails_short_id")
    else
        # Version transform from tagged version to asset version
        # 11.11.0-rc1.ee.0 -> v11-11-0-rc1-ee
        # 11.10.4.ee.0 -> v11-10-4-ee

        # Remove the .0 from the end of the package if it exists
        assets_version="${DEPLOY_VERSION%.0}"
        # Convert dots to dashes and prefix with 'v'
        assets_version="v${assets_version//./-}"
    fi

    assets_image="${DEV_ASSETS_IMAGE:-gitlab/gitlab-ee/gitlab-assets-ee}"
    assets_registry="${DEV_ASSETS_REGISTRY:-dev.gitlab.org:5005}"

    set +x
    docker login -u "$DEV_ASSETS_DEPLOY_TOKEN_USER" -p "$DEV_ASSETS_DEPLOY_TOKEN" "$assets_registry" >/dev/null 2>&1
    set -x

    debug_seconds

    assets_image_name="${assets_registry}/${assets_image}:${assets_version}"
    if ! docker pull "${assets_image_name}"; then
      debug_seconds
      echo "${assets_image_name} is not available, aborting"
      return 1
    fi
    debug_seconds

    docker create --name asset_cache "${assets_image_name}"
    docker cp asset_cache:/assets "$tmpdir"
    debug_seconds
}

decompress_missing_assets() {
    # Extract any gzipped asset which does not have a non-gzipped counterpart
    # These are sometimes omitted in omnibus packages to reduce package size
    set +x
    find "$tmpdir/assets" -name '*.gz' -type f -print0 | while IFS= read -r -d $'\0' gzipped_file; do
        if [[ ! -f "${gzipped_file%.gz}" ]]; then
            echo "Restoring ${gzipped_file%.gz}"
            gzip -dk "$gzipped_file"
        fi
    done
    set -x
    debug_seconds
}

upload_assets() {
    if [[ "${CHECKMODE:-false}" == "true" ]]; then
      return
    fi

    set +x
    gsutil_opts=('-m' '-q')
    if [[ -n $DEV_ASSETS_SERVICE_ACCOUNT_KEY ]]; then
        (umask 077 && echo "$DEV_ASSETS_SERVICE_ACCOUNT_KEY" | base64 -d > /credfile.json)
        gsutil_opts+=('-o' 'Credentials:gs_service_key_file=/credfile.json')
    fi
    set -x

    gsutil "${gsutil_opts[@]}" -h 'Cache-Control:public,max-age=31536000' cp -a public-read -r "$tmpdir/assets/" "gs://gitlab-$env-assets"
    debug_seconds

    # write out an info file for recording the version and checking the health of the
    # asset bucket
    info_file=$(mktemp)
    echo "{\"msg\": \"Successful update on $(date -u)\", \"version\": \"$DEPLOY_VERSION\"}" > "$info_file"
    gsutil "${gsutil_opts[@]}" -h "Content-Type:application/json" -h "Cache-Control:private,max-age=0" cp -a public-read "$info_file" "gs://gitlab-$env-assets/$info_path"
}

setup() {
    if [[ -z $DEV_ASSETS_DEPLOY_TOKEN_USER ||
          -z $DEV_ASSETS_DEPLOY_TOKEN ||
          -z $DEPLOY_VERSION ||
          -z $CURRENT_DEPLOY_ENVIRONMENT ||
          -z $PACKAGECLOUD_TOKEN ]]; then
        echo "You must define these vars to use this script!
              DEV_ASSETS_DEPLOY_TOKEN_USER
              DEV_ASSETS_DEPLOY_TOKEN
              DEPLOY_VERSION
              CURRENT_DEPLOY_ENVIRONMENT
              PACKAGECLOUD_TOKEN"
        exit 1
    fi

    CI_PROJECT_DIR=${CI_PROJECT_DIR:-./}
    tmpdir="$(mktemp -d)"
    env="$CURRENT_DEPLOY_ENVIRONMENT"
    mkdir -p "$tmpdir"
    # path to the info json file that will be written
    # when asset uploading completes
    info_path=${ASSET_INFO_PATH:-info}
}


main() {
    setup

    if ! fetch_from_registry; then
        echo "Fetching from registry not available, fetching assets from package instead"
        fetch_from_pkg
    fi
    decompress_missing_assets

    upload_assets
}

main
