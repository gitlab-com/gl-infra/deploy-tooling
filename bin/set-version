#!/bin/bash
# shellcheck disable=SC1091

# Set GitLab version attribute for a Chef Role (e.g. gstg-omnibus-version)

set -euf -o pipefail

#######################################
# Print out a helper
#######################################
usage() {
  cat <<EOF
Sets the GitLab version attribute in the Omnibus version Chef role. (e.g. gstg-omnibus-version)
This is done at the end of the deployment so that the version matches what was installed by Ansible.
Environment variables:
  CHEF_USER
  CHEF_PEM_KEY
  CURRENT_DEPLOY_ENVIRONMENT_STAGE
  DEPLOY_VERSION
  CHEF_SERVER_URL (Optional)
  CHECKMODE       (Optional)
EOF
}

#######################################
# Set version attribute of a Chef role. When CHECKMODE=true, print out without making the actual change
# Globals:
#   CHECKMODE
#   DEPLOY_VERSION
#   CURRENT_DIR
# Outputs:
#   Version attribute of the Chef role is changed.
#######################################
set_version() {
  current_version=$(get_knife_role_attribute "version")
  log_info "Current version is: ${current_version}"

  if [[ "${CHECKMODE}" = "true" ]]; then
    log_info "Would set version attribute of ${ROLE_NAME} to '${DEPLOY_VERSION}', currently set to '${current_version}'. Not doing anything because CHECKMODE is set."
    return 0
  fi

  log_info "Setting ${ROLE_NAME} to version ${DEPLOY_VERSION}"
  set_knife_role_attribute "version" "${DEPLOY_VERSION}"
}

# Import the helper functions
source "$(dirname "$0")/chef-knife-helper.sh"

required_env='CHEF_USER CHEF_PEM_KEY CURRENT_DEPLOY_ENVIRONMENT_STAGE DEPLOY_VERSION'
for name in $required_env; do
  if [[ -z "${!name}" ]]; then
    log_error "Error: $name is not set. Exiting with an error."
    usage
    exit 1
  fi
done

write_client_key
write_knife_config_file
log_info "Role information BEFORE changing:"
role_exist
set_version
log_info "Role information AFTER changing:"
role_exist
