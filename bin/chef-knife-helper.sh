#!/bin/bash

ROLE_NAME="$CURRENT_DEPLOY_ENVIRONMENT_STAGE-omnibus-version"
CURRENT_DIR=$(pwd)
CONFIG_FILE_NAME="${CURRENT_DIR}/knife.rb"
CLIENT_KEY_FILE_NAME="${CURRENT_DIR}/chef-server.key"
CHEF_SERVER_URL="${CHEF_SERVER_URL:-https://chef.gitlab.com/organizations/gitlab}"
CHEF_USER="${CHEF_USER:-$USER}"
CHECKMODE="${CHECKMODE:-false}"

#######################################
# Check if a Chef Role exists
# Globals:
#   ROLE_NAME
#   CURRENT_DIR
#######################################
role_exist() {
  if ! knife role show  "${ROLE_NAME}" --config "${CONFIG_FILE_NAME}" ; then
    log_error "Role ${ROLE_NAME} does not exist. Exiting with an error."
    exit 1
  fi
}

#######################################
# Set an Chef Role's attribute
# Globals:
#   ROLE_NAME
#   CURRENT_DIR
#   CONFIG_FILE_NAME
# Arguments:
#   $1: attribute name
#   $2: attribute value
#######################################
set_knife_role_attribute() {
  local attribute
  local value
  attribute=$1
  value=$2
  if [[ "${value}" != "true" ]] && [[ "${value}" != "false" ]]; then
    value="\"${value}\""
  fi
  if ! knife exec --config "${CONFIG_FILE_NAME}" -E "roles.find(:name => '${ROLE_NAME}') { |role| role.default_attributes['omnibus-gitlab']['package']['${attribute}']=${value}; role.save;}"; then
    log_error "Set attribute \"${attribute}\" failed. Exiting with an error."
    exit 1
  else
    log_info "Saved attribute '${attribute}' to '${value}'."
  fi
}

#######################################
# Get an Chef Role's attribute
# Globals:
#   ROLE_NAME
#   CURRENT_DIR
#   CONFIG_FILE_NAME
# Arguments:
#   $1: attribute name
#######################################
get_knife_role_attribute() {
  local attribute
  attribute=$1
  knife exec --config "${CONFIG_FILE_NAME}" -E "roles.find(:name => '${ROLE_NAME}') { |role| puts role.default_attributes['omnibus-gitlab']['package']['${attribute}'];}"
}

#######################################
# Prepare the knife config file to use in the program
#######################################
write_knife_config_file() {
  cat <<KNIFE_RB > "${CONFIG_FILE_NAME}"
log_level                :info
log_location             STDOUT
node_name                '${CHEF_USER}'
client_key               "${CLIENT_KEY_FILE_NAME}"
chef_server_url          '${CHEF_SERVER_URL}'
cache_type               'BasicFile'

knife[:vault_mode] = 'client'
knife[:ssh_user] = '${CHEF_USER}'
KNIFE_RB
}

#######################################
# Prepare Chef Infra Server API client key file
#######################################
write_client_key() {
  echo "${CHEF_PEM_KEY}" > "${CLIENT_KEY_FILE_NAME}"
}

#######################################
# Function to format and log messages
#######################################

log_message() {
    local log_level="$1"
    local message="$2"
    local current_time
    current_time=$(date "+%H:%M:%S")
    echo "[$current_time] [$log_level] $message"
}

#######################################
# Function for INFO log level
#######################################
log_info() {
    log_message "INFO" "$1"
}

#######################################
# Function for ERROR log level
#######################################
log_error() {
    log_message "ERROR" "$1" >&2
}