UNAME := $(shell uname -s)

.PHONY: help
help: ### This help screen.
ifeq ($(UNAME), Linux)
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
else
	@awk -F ':.*###' '$$0 ~ FS {printf "%15s%s\n", $$1 ":", $$2}' $(MAKEFILE_LIST) | grep -v '@awk' | sort
endif

# This doesn't matter for the gitlab registry
# when using a personal access token
REGISTRY_ACCESS_USER ?= gl-infra

build-docker-ci: ### Build Docker images for deploy tooling
	docker build -t registry.ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/ci-image-py3 -f Dockerfile-ci .

push-docker-ci: build-docker-ci ### Push Docker images for deploy tooling
ifndef REGISTRY_ACCESS_TOKEN
    $(error REGISTRY_ACCESS_TOKEN is required to push to the gitlab registry)
endif
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.ops.gitlab.net
	docker push registry.ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/ci-image-py3

get-inventory: ### Lists inventory detected via the GCP inventory Plugin
	ansible-inventory --list all
