# One-off Commands
This directory contains one-off plays for maintenance that can be
run on the fleet in the deployer pipeline.

The name of the file should match the play name that is used
you would like to execute. For example, if your command is ```hostname```,
the playbook should be ```hostname.yml```.

## Commands
Reference the [repository listing](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/tree/master/cmds) or the output of `/chatops run deploycmd --list` for the current list of commands

## Testing
Once you have set up your pipenv per the project README, you can test commands
like this:
```
export CMD=hostname
export CURRENT_DEPLOY_ENVIRONMENT=gstg
export GITLAB_ROLES=type_gitaly:&env_gprd
ansible-playbook --check cmd.yml"
```
This example runs the hostname command on the web pages nodes in staging.

## Intent
The goal of these commands is to be called via triggers in the
[deployer](https://gitlab.com/gitlab-org/release/docs/blob/master) CI
pipelines.
